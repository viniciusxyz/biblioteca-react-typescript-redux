import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import ContainerLivros from './componentes/ContainerLivros'
import HeaderBiblioteca from './componentes/HeaderBiblioteca'
import RelatorioCompra from './componentes/RelatorioCompra'
import { connect } from 'react-redux';


interface IProp extends React.Props<any> {
  ReducerCarrinho: object[],
  ReducerValor: number
}

class App extends React.Component<IProp,{}> {
  public render() {
    return (
      <div >
        <CssBaseline />
        <HeaderBiblioteca />
        <ContainerLivros />
        <RelatorioCompra/>
      </div>
    );
  }

}

const mapStateToProps = (store:any) => ({
  ReducerCarrinho: store.ReducerCarrinho,
  ReducerValor: store.ReducerValor
});

export default connect(mapStateToProps)(App);
