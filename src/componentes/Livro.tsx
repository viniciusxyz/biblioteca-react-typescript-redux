import * as React from 'react'
import { Card, Typography, Button } from '@material-ui/core';
import {connect} from 'react-redux'
import {bindActionCreators, Dispatch} from 'redux'
import ActionAddCarrinho from '../actions/ActionAddCarrinho'
import ActionAddValor from '../actions/ActionAddValor'

// tslint:disable-next-line:interface-name
interface IPropsParameters extends React.Props<any> {
    ObjLivro: object,
    TituloLivro: string,
    Autor: string
    Conteudo: string
    Imagem: string
    Valor: string,
}

interface IPropsRedux extends React.Props<any>
{
    // tslint:disable-next-line:ban-types
    AddLivro: Function,
    // tslint:disable-next-line:ban-types
    AddValor: Function    
}

class Livro extends React.Component<IPropsParameters & IPropsRedux, any>
{

    constructor(props: any) {
        super(props);
        this.state = {
            NroCompra: 0
        };
        this.addQtdeLivro = this.addQtdeLivro.bind(this);
    }

    public styleCardLivro: React.CSSProperties = {
        display: 'inline-block',
        margin: 15,
        padding: 10,
        height: 300
    }

    public styleImgLivro: React.CSSProperties = {
        width: '60%',
        padding: 15,
        margin: '0 auto',
        display: 'block',
    }

    public visibleQtdComprado = "none"

    public tipoTitulo: React.CSSProperties = {
        fontSize: 20,
    }

    public tipoPreco: React.CSSProperties = {
        fontSize: 15
    }

    public addQtdeLivro(event: any): void {
        this.setState({
            NroCompra: this.state['NroCompra'] + 1
        })
        this.props.AddLivro(this.props.ObjLivro)
        this.props.AddValor(this.props.ObjLivro['Valor'])
    }

    public render() {

        if (this.state['NroCompra'] > 0) {
            this.visibleQtdComprado = "block"
        }

        const StyleQtdCompra: React.CSSProperties = {
            float: 'right',
            padding: 7,
            borderRadius: '100%',
            display: this.visibleQtdComprado,
            backgroundColor: 'red',
            color: '#fff',
        }

        const graphImage = require('../images/' + this.props.Imagem)

        return (
            <Card style={this.styleCardLivro} elevation={1}>
                <Typography style={this.tipoTitulo}>
                    {this.props.TituloLivro}
                </ Typography>

                <img src={graphImage} style={this.styleImgLivro} />

                <Typography style={this.tipoPreco}>
                    Preço: {this.props.Valor}
                </ Typography>

                <Button
                    onClick={this.addQtdeLivro}
                    variant="contained"
                    style={{ justifyContent: 'flex-end' }}
                >
                    Adquirir
                </Button>
                <div style={StyleQtdCompra}>
                    {this.state["NroCompra"]}
                </div>

            </Card>
        )
    }
}

const mapStateToProps = (store:any) => ({
    ReducerCarrinho: store.ReducerCarrinho
  });
  
  const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    AddLivro: ActionAddCarrinho,  // Adiciona função AddItem adicionada ao contexto com o conteúdo de ActionAddCarrinho
    AddValor: ActionAddValor
  }, dispatch);
  
  
  export default connect<{},{},IPropsParameters>(mapStateToProps,mapDispatchToProps)(Livro);