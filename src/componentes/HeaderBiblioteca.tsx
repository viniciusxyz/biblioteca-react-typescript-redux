import * as React from 'react'
import CarrinhoCompras from '../images/carrinho.png'
import { connect } from 'react-redux';

// Define propriedades da classe para que elas possam ser acessadas através do this.props
interface Iprops extends React.Props<any>
{
    ReducerCarrinho: object[]
}

class HeaderBiblioteca extends React.Component<Iprops,{}>
{
    public TotalItensCarrinho: 0
    

    public styleHeader: React.CSSProperties = {
        width: '100%',
        height: 100,
        borderRadius: 0,
        backgroundColor: 'rgb(244,177,131)',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }


    public render()
    {
        console.log("Props")
        console.log(this.props)
        console.log("State")
        console.log(this.state)
        return (
            <div style={this.styleHeader}>
                <img src={CarrinhoCompras} style={{width:60,margin:15}}/>

                <div style={{backgroundColor:'red',padding:10,color: 'white',display:'inline-block',borderRadius:'50%',position:'absolute',marginTop:-15,marginLeft:55}}>
                    {this.props.ReducerCarrinho.length}
                </div>
            </div>
        )
    }
}

// Realiza conexão entre a Store e as props
const mapStateToProps = (store:any) => ({
    ReducerCarrinho: store.ReducerCarrinho
 });

export default connect(mapStateToProps)(HeaderBiblioteca);