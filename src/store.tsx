import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'; // Adiciona componente para DevTools Redux
import reducers from './reducers';


const store = createStore(reducers,composeWithDevTools());

export default store;
